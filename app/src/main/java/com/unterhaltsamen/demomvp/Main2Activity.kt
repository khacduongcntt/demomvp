package com.unterhaltsamen.demomvp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.unterhaltsamen.demomvp.calculate.CalculatePresenter
import com.unterhaltsamen.demomvp.calculate.PresenterCalculate
import kotlinx.android.synthetic.main.activity_main.*

class Main2Activity : AppCompatActivity(),CalculatePresenter.View {
    override fun updateResult(result: Int) {
        txt_ketqua.setText(result.toString())
    }
    internal var presenterCalculate : PresenterCalculate?= PresenterCalculate(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_cong.setOnClickListener {
            var sothu_1 : Int = (edt_so1.text).toString().toInt()
            var sothu_2 : Int = (edt_so2.text).toString().toInt()
            /**
             * call presenter -> request hanlder calculate
             */
            presenterCalculate?.presentercalculate(sothu_1,sothu_2,1)
        }
        btn_chia.setOnClickListener {
            var sothu_1 : Int = (edt_so1.text).toString().toInt()
            var sothu_2 : Int = (edt_so2.text).toString().toInt()
            /**
             * call presenter -> request hanlder calculate
             */
            presenterCalculate?.presentercalculate(sothu_1,sothu_2,4)

        }
        btn_chu.setOnClickListener {
            var sothu_1 : Int = (edt_so1.text).toString().toInt()
            var sothu_2 : Int = (edt_so2.text).toString().toInt()
            /**
             * call presenter -> request hanlder calculate
             */
            presenterCalculate?.presentercalculate(sothu_1,sothu_2,2)

        }
        btn_nhan.setOnClickListener {
            var sothu_1 : Int = (edt_so1.text).toString().toInt()
            var sothu_2 : Int = (edt_so2.text).toString().toInt()
            /**
             * call presenter -> request hanlder calculate
             */
            presenterCalculate?.presentercalculate(sothu_1,sothu_2,3)

        }
    }
}
