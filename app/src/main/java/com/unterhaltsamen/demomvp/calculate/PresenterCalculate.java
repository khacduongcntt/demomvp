package com.unterhaltsamen.demomvp.calculate;

public class PresenterCalculate implements CalculatePresenter.Presenter {


    private HanderCalculate handerCalculate;
    private CalculatePresenter.View view;


    public PresenterCalculate(CalculatePresenter.View view) {
        this.view = view;
    }
    // view call halder calculate
    public void presentercalculate(int soA,int soB,int styleCalculate){
        handerCalculate = new HanderCalculate(this);
        handerCalculate.handerCalculate(soA,soB,styleCalculate);
    }

    // gửi kết quả update view
    @Override
    public void send(int result) {
        view.updateResult(result);
    }
}
