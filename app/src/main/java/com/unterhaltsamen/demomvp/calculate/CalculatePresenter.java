package com.unterhaltsamen.demomvp.calculate;

public interface CalculatePresenter {

    interface View{
        void updateResult(int result);
    }
    interface Presenter {
        void send(int result);
    }
}
