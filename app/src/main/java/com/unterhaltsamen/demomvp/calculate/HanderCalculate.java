package com.unterhaltsamen.demomvp.calculate;

public class HanderCalculate {
    private CalculatePresenter.Presenter view;

    public HanderCalculate(CalculatePresenter.Presenter view) {
        this.view = view;
    }

    /**
     *  xử lý là chả kết quả về presenter
     * @param soA
     * @param soB
     * @param styleCalculate
     */
    public void handerCalculate(int soA, int soB, int styleCalculate) {
        int result;
        if (styleCalculate == 1) {
            result = soA + soB;
        } else if (styleCalculate == 2) {
            result = soA - soB;
        } else if (styleCalculate == 3) {
            result = soA * soB;
        } else {
            result = soA / soB;
        }
        //xử lý là chả kết quả về presenter
        view.send(result);
    }
}
